<?php

declare(strict_types=1);

namespace App\Modules\Admin\Aplicacion;

use App\Modules\Admin\Dominio\CuentaEntity;
use App\Modules\Admin\Infraestructura\CuentaRepository;
use Illuminate\Validation\ValidationException;

class CuentaService
{
    public function __construct(protected CuentaRepository $repository) {}

    public function index()
    {
        return $this->repository->index();
    }

    public function search(int $id): ?object
    {
        return $this->repository->search($id);
    }

    /**
     * @throws ValidationException
     */
    public function store(object $request): ?object
    {
        return $this->repository->store((new CuentaEntity($request))->toStore());
    }

    /**
     * @throws ValidationException
     */
    public function update(int $id, object $request): void
    {
        $request->idCuenta = $id;
        $this->repository->update($id, (new CuentaEntity($request))->toUpdate());
    }

    /**
     * @throws ValidationException
     */
    public function delete(int $id): void
    {
        (new CuentaEntity((object)[]))->toDelete($id);
        $this->repository->delete($id);
    }
}
