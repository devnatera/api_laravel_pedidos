<?php

declare(strict_types=1);

namespace App\Modules\Admin\Infraestructura;

use App\Modules\Admin\Interfaces\InterfaceBaseRepository;

abstract class BaseRepository implements InterfaceBaseRepository
{
    private mixed $model;

    public function __construct()
    {
        $this->model = $this->getModel();
    }

    abstract protected function getModel();

    public function store(array $data): ?object
    {
        return $this->model->create($data);
    }
}
