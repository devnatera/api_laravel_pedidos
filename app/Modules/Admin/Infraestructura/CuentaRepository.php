<?php

declare(strict_types=1);

namespace App\Modules\Admin\Infraestructura;

use App\Models\Admin\Cuenta;
use Illuminate\Support\Collection;

class CuentaRepository extends BaseRepository
{
    public function getModel(): Cuenta
    {
        return (new Cuenta());
    }

    public function index(): Collection
    {
        return Cuenta::all();
    }

    public function search(int $id): ?object
    {
        $cuenta = Cuenta::where('idCuenta', $id)->first();
        return ($cuenta)? (object)$cuenta->toArray(): null;
    }

    public function searchWhere(object $queryParams): ?object
    {
        $cuenta = Cuenta::select()
            ->when(isset($queryParams->email), fn($query) => $query->where('email', $queryParams->email))
            ->first();
        return ($cuenta)? (object)$cuenta->toArray(): null;
    }

    public function update(int $id, array $data): void
    {
        Cuenta::where('idCuenta', $id)->update($data);
    }

    public function delete(int $id): void
    {
        Cuenta::where('idCuenta', $id)->delete();
    }
}
