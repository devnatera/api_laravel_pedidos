<?php
declare(strict_types=1);
namespace App\Modules\Admin\Dominio;

class Entity
{
    private object $request;

    public function __construct(object $request){
        $this->request = $request;
    }
}