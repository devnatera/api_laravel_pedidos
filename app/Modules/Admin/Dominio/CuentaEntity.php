<?php
declare(strict_types=1);
namespace App\Modules\Admin\Dominio;

use App\Modules\Admin\Infraestructura\CuentaRepository;
use Illuminate\Validation\ValidationException;

class CuentaEntity extends Entity
{
    private object $request;
    public function __construct(object $request){
        $this->request = $request;
        parent::__construct($request);
    }

    /**
     * @throws ValidationException
     */
    public function toStore(): array
    {
        return array(
            'nombre' => self::nombre(),
            'email' => self::storeEmail(),
            'telefono' => self::telefono()
        );
    }

    /**
     * @throws ValidationException
     */
    public function toUpdate(): array
    {
        if (! $this->exists($this->request->idCuenta))
            throw ValidationException::withMessages([
                'message' => 'la cuenta '. $this->request->idCuenta . ' no existe!'
            ]);
        return array(
            'nombre' => self::nombre(),
            'email' => self::updateEmail(),
            'telefono' => self::telefono()
        );
    }

    /**
     * @throws ValidationException
     */
    public function toDelete($id): void
    {
        if (! $this->exists($id))
            throw ValidationException::withMessages([
                'message' => 'la cuenta '. $id . ' no existe!'
            ]);
    }

    /**
     * @throws ValidationException
     */
    private function nombre(): string
    {
        if (!property_exists($this->request, 'nombre'))
            throw ValidationException::withMessages([
                'message' => 'El campo nombre debe estar presente.'
            ]);
        if (!$this->request->nombre)
            throw ValidationException::withMessages([
                'message' => 'El campo nombre es requerido.'
            ]);
        return $this->request->nombre;
    }

    /**
     * @throws ValidationException
     */
    private function telefono(): ?string
    {
        return $this->request->telefono;
    }

    /**
     * @throws ValidationException
     */
    private function storeEmail(): string
    {
        $response = self::unique();
        if ($response)
            throw ValidationException::withMessages([
                'message' => 'El email ya existe.'
            ]);
        return $this->request->email;
    }

    /**
     * @throws ValidationException
     */
    private function updateEmail(): string
    {
        $response = self::unique();
        if ($response)
            if ($response->idCuenta != $this->request->idCuenta)
                throw ValidationException::withMessages([
                    'message' => 'El email ya existe.'
                ]);
        return $this->request->email;
    }

    /**
     * @throws ValidationException
     */
    private function unique(): ?object
    {
        return (new CuentaRepository())->searchWhere((object)[
            'email' => $this->request->email
        ]);
    }

    private function exists($id): bool
    {
        $response = (new CuentaRepository())->search($id);
        return isset($response);
    }
}