<?php
declare(strict_types=1);
namespace App\Modules\Pedidos\Interfaces;

interface InterfaceBaseRepository
{
    public function store(array $data);
    public function update(int $id, array $data);
}