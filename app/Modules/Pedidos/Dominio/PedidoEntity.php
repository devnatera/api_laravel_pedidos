<?php
declare(strict_types=1);
namespace App\Modules\Pedidos\Dominio;

use App\Modules\Admin\Infraestructura\CuentaRepository;
use App\Modules\Pedidos\Infraestructura\PedidoRepository;
use Illuminate\Validation\ValidationException;

class PedidoEntity extends Entity
{
    private object $request;
    public function __construct(object $request){
        $this->request = $request;
        parent::__construct($request);
    }

    /**
     * @throws ValidationException
     */
    public function toStore(): array
    {
        return array(
            'idCuenta' => self::idCuenta(),
            'producto' => self::producto(),
            'cantidad' => self::cantidad(),
            'valor' => self::valor(),
            'total' => self::total(),
        );
    }

    /**
     * @throws ValidationException
     */
    public function toUpdate(): array
    {
        if (! $this->exists($this->request->idCuenta))
            throw ValidationException::withMessages([
                'message' => 'el pedido '. $this->request->idCuenta . ' no existe!'
            ]);
        self::idCuenta();
        return array(
            'producto' => self::producto(),
            'cantidad' => self::cantidad(),
            'valor' => self::valor(),
            'total' => self::total(),
        );
    }

    /**
     * @throws ValidationException
     */
    public function toDelete($id): void
    {
        if (! $this->exists($id))
            throw ValidationException::withMessages([
                'message' => 'el pedido '. $id . ' no existe!'
            ]);
    }

    /**
     * @throws ValidationException
     */
    private function idCuenta(): int
    {
        $cuenta = (new CuentaRepository)->search((int)$this->request->idCuenta);
        if (!$cuenta)
            throw ValidationException::withMessages([
                'message' => 'la cuenta '. $this->request->idCuenta . ' no existe!'
            ]);
        return (int)$this->request->idCuenta;
    }

    /**
     * @throws ValidationException
     */
    private function producto(): string
    {
        if (!property_exists($this->request, 'producto'))
            throw ValidationException::withMessages([
                'message' => 'El campo producto debe estar presente.'
            ]);
        if (!$this->request->producto)
            throw ValidationException::withMessages([
                'message' => 'El campo producto es requerido.'
            ]);
        return $this->request->producto;
    }

    /**
     * @throws ValidationException
     */
    private function cantidad(): float
    {
        if (!property_exists($this->request, 'cantidad'))
            throw ValidationException::withMessages([
                'message' => 'El campo cantidad debe estar presente.'
            ]);
        if (!$this->request->cantidad)
            throw ValidationException::withMessages([
                'message' => 'El campo cantidad es requerido.'
            ]);
        $this->isNumeric($this->request->cantidad, 'cantidad');
        return (float)$this->request->cantidad;
    }

    /**
     * @throws ValidationException
     */
    private function valor(): float
    {
        if (!property_exists($this->request, 'valor'))
            throw ValidationException::withMessages([
                'message' => 'El campo valor debe estar presente.'
            ]);
        if (!$this->request->valor)
            throw ValidationException::withMessages([
                'message' => 'El campo valor es requerido.'
            ]);
        $this->isNumeric($this->request->valor, 'valor');
        return (float)$this->request->valor;
    }

    /**
     * @throws ValidationException
     */
    private function total(): float
    {
        if (!property_exists($this->request, 'total'))
            throw ValidationException::withMessages([
                'message' => 'El campo total debe estar presente.'
            ]);
        if (!$this->request->total)
            throw ValidationException::withMessages([
                'message' => 'El campo total es requerido.'
            ]);

        $this->isNumeric($this->request->total, 'total');

        if ((float)$this->request->total != (self::cantidad() * self::valor()))
            throw ValidationException::withMessages([
                'message' => "El valor total está mal calculado"
            ]);
        return (float)$this->request->total;
    }

    private function exists($id): bool
    {
        $response = (new PedidoRepository())->search($id);
        return isset($response);
    }

    /**
     * @throws ValidationException
     */
    private function isNumeric($valor, string $campo) {
        if (!is_numeric($valor))
            throw ValidationException::withMessages([
                'message' => "El campo $campo debe ser numerico"
            ]);
    }
}