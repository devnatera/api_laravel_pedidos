<?php
declare(strict_types=1);
namespace App\Modules\Pedidos\Dominio;

class Entity
{
    private object $request;

    public function __construct(object $request){
        $this->request = $request;
    }
}