<?php

declare(strict_types=1);

namespace App\Modules\Pedidos\Aplicacion;

use App\Modules\Pedidos\Dominio\PedidoEntity;
use App\Modules\Pedidos\Infraestructura\PedidoRepository;
use Illuminate\Validation\ValidationException;

class PedidoService
{
    public function __construct(protected PedidoRepository $repository) {}

    public function index()
    {
        return $this->repository->index();
    }

    public function search(int $id): ?object
    {
        return $this->repository->search($id);
    }

    /**
     * @throws ValidationException
     */
    public function store(object $request): ?object
    {
        $store = $this->repository->store((new PedidoEntity($request))->toStore());
        if (!$store)
            return $store;
        return $this->search($store->idPedido);
    }

    /**
     * @throws ValidationException
     */
    public function update(int $id, object $request): void
    {
        $request->idCuenta = $id;
        $this->repository->update($id, (new PedidoEntity($request))->toUpdate());
    }

    /**
     * @throws ValidationException
     */
    public function delete(int $id): void
    {
        (new PedidoEntity((object)[]))->toDelete($id);
        $this->repository->delete($id);
    }
}
