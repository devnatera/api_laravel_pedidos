<?php

declare(strict_types=1);

namespace App\Modules\Pedidos\Infraestructura;

use App\Models\Pedidos\Pedido;
use App\Modules\Admin\Infraestructura\CuentaRepository;
use Illuminate\Support\Collection;

class PedidoRepository extends BaseRepository
{
    public function getModel(): Pedido
    {
        return (new Pedido());
    }

    public function index(): Collection
    {
        return Pedido::all();
    }

    public function search(int $id): ?object
    {
        $pedido = (object)Pedido::where('idPedido', $id)->first()?->toArray();
        if (!$pedido) return null;
        $pedido->cliente = (new CuentaRepository())->search((int)$pedido->idCuenta);
        return $pedido;
    }

    public function searchWhere(object $queryParams): ?object
    {
        return (object)Pedido::select()
            ->when(isset($queryParams->email), fn($query) => $query->where('email', $queryParams->email))
            ->first()
            ?->toArray();
    }

    public function update(int $id, array $data): void
    {
        Pedido::where('idPedido', $id)->update($data);
    }

    public function delete(int $id): void
    {
        Pedido::where('idPedido', $id)->delete();
    }
}
