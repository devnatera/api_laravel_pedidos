<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Admin\Aplicacion\CuentaService;

class CuentaController extends Controller
{
    public function __construct(
        protected CuentaService $service
    ) {
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        $response = $this->service->index();
        return response()->json([
            'message' => 'Listado de recursos.',
            'data' => $response,
        ]);
    }

    public function search(int $id): \Illuminate\Http\JsonResponse
    {
        $response = $this->service->search($id);
        return response()->json([
            'message' => 'Recurso',
            'data' => $response,
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $response = $this->service->store((object)$request->all());
        return response()->json([
            'message' => 'Recurso creado exitosamente.',
            'data' => $response
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function update(int $id, Request $request): \Illuminate\Http\JsonResponse
    {
        $this->service->update($id, (object)$request->all());
        return response()->json([
            'message' => 'Recurso actualizado exitosamente.',
            'data' => true
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function delete(int $id): \Illuminate\Http\JsonResponse
    {
        $this->service->delete($id);
        return response()->json([
            'message' => 'Recurso eliminado exitosamente.',
            'data' => true,
        ]);
    }
}
