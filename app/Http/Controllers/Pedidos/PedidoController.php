<?php

namespace App\Http\Controllers\Pedidos;

use App\Http\Controllers\Controller;
use App\Modules\Admin\Aplicacion\CuentaService;
use App\Modules\Pedidos\Aplicacion\PedidoService;
use App\Notification\Aplicacion\HttpNotificacionService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PedidoController extends Controller
{
    public function __construct(
        protected PedidoService $service,
        protected HttpNotificacionService $notificacionService,
        protected CuentaService $cuentaService
    ) {
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        $response = $this->service->index();
        return response()->json([
            'message' => 'Listado de recursos.',
            'data' => $response,
        ]);
    }

    public function search(int $id): \Illuminate\Http\JsonResponse
    {
        $response = $this->service->search($id);
        return response()->json([
            'message' => 'Recurso',
            'data' => $response,
        ]);
    }

    /**
     * @return View\Factory|View\View|Application
     */
    public function create(): View\Factory|View\View|Application
    {
        $cuentas = $this->cuentaService->index();
        return view('welcome', compact('cuentas'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = (object)$request->all();
        $response = $this->service->store($data);
        $notificacion = ($data->notifywebsocket)
            ? ''
            : $this->notificacionService->notify((array)$response);

        return response()->json([
            'message' => 'Recurso creado exitosamente.',
            'data' => $response,
            'notificacion nuevo pedido' => $notificacion
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function update(int $id, Request $request): \Illuminate\Http\JsonResponse
    {
        $this->service->update($id, (object)$request->all());
        return response()->json([
            'message' => 'Recurso actualizado exitosamente.',
            'data' => true
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function delete(int $id): \Illuminate\Http\JsonResponse
    {
        $this->service->delete($id);
        return response()->json([
            'message' => 'Recurso eliminado exitosamente.',
            'data' => true,
        ]);
    }
}
