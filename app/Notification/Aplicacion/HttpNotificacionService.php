<?php

namespace App\Notification\Aplicacion;

use App\Notification\Interfaces\InterfaceBaseNotificacion;

class HttpNotificacionService implements InterfaceBaseNotificacion
{

    public function notify(array $data): object
    {
        $response = (new HttpClientService())->post($data);

        if ($response->successful)
            return (object)['notificado' => true];

        return (object)[
            'notificado' => false,
            'error' => 'Error al notificar el pedido'
        ];
    }
}
