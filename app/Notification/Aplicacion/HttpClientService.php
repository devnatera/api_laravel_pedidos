<?php

namespace App\Notification\Aplicacion;

use Illuminate\Support\Facades\Http;

class HttpClientService
{

    function post($body): object
    {
        $response = Http::withHeaders([
                'Content-Type' => 'application/json',
            ])
            ->post(env('API_NOTIFICACION_URL'), $body);

        return (object) [
            'successful' => $response->successful(),
            'data' => $response->json()
        ];
    }
}
