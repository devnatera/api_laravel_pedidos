<?php

namespace App\Notification\Interfaces;

interface InterfaceBaseNotificacion
{
    public function notify(array $data): object;
}
