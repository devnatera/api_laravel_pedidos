<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    use HasFactory;

    protected $primaryKey = 'idCuenta';

    protected $fillable = [
        'idCuenta',
        'nombre',
        'email',
        'teléfono',
    ];
}
