<?php

namespace App\Models\Pedidos;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;

    protected $primaryKey = 'idPedido';

    protected $fillable = [
        'idCuenta',
        'idPedido',
        'producto',
        'cantidad',
        'valor',
        'total',
    ];
}
