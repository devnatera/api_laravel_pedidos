<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->unsignedBigInteger('idCuenta');
            $table->foreign('idCuenta')->references('idCuenta')->on('cuentas');
            $table->bigIncrements('idPedido');
            $table->string('producto');
            $table->decimal('cantidad', 12, 4);
            $table->decimal('valor', 12, 4);
            $table->decimal('total', 12, 4);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pedidos');
    }
};
