<?php

use App\Http\Controllers\Admin\CuentaController;
use App\Http\Controllers\Pedidos\PedidoController;
use App\Http\Middleware\ForceJsonResponse;
use App\Http\Middleware\CorsAllow;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware([ForceJsonResponse::class, CorsAllow::class])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('cuentas', [CuentaController::class, 'index'])->name('cuentas.index');
        Route::get('cuentas/{id}', [CuentaController::class, 'search'])->name('cuentas.search');
        Route::post('cuentas', [CuentaController::class, 'store'])->name('cuentas.store');
        Route::put('cuentas/{id}', [CuentaController::class, 'update'])->name('cuentas.update');
        Route::delete('cuentas/{id}', [CuentaController::class, 'delete'])->name('cuentas.delete');
    });
    Route::get('pedidos', [PedidoController::class, 'index'])->name('pedidos.index');
    Route::get('pedidos/{id}', [PedidoController::class, 'search'])->name('pedidos.search');
    Route::post('pedidos', [PedidoController::class, 'store'])->name('pedidos.store');
    Route::put('pedidos/{id}', [PedidoController::class, 'update'])->name('pedidos.update');
    Route::delete('pedidos/{id}', [PedidoController::class, 'delete'])->name('pedidos.delete');
});
