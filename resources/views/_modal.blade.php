<div id="modal" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header p-2">
        <h5 id="modal-title" class="modal-title">Notificación</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body p-2">
        <p id="modal-text">...</p>
      </div>
      <div class="modal-footer p-2">
        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script>
  function errorNotification(message) {
    $('#modal-title').text('Error').addClass('text-danger');
    $('#modal-text').text(message);
    $('#modal').modal('show');
  }
  function successNotification(message) {
    $('#modal-title').text('Notificación').addClass('text-dark');
    $('#modal-text').text(message);
    $('#modal').modal('show');
  }
</script>