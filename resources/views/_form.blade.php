<form action="{{route('pedidos.store')}}" id="form_pedido" method="POST" class="w-100">
  @csrf
  <div class="row">
    <div class="form-group col-6 mb-2">
      <label for="idCuenta" class="form-label mb-0">Cuenta</label>
      <select name="idCuenta" id="idCuenta" class="form-select" required>
        <option>Seleccione ...</option>
        @foreach($cuentas as $cuenta)
          <option value="{{$cuenta->idCuenta}}">{{$cuenta->nombre}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group col-6 mb-2">
      <label for="producto" class="form-label mb-0">Producto</label>
      <input type="text" name="producto" id="producto" class="form-control" required>
    </div>
    <div class="form-group col-6 mb-2">
      <label for="cantidad" class="form-label mb-0">Cantidad</label>
      <input type="number" name="cantidad" id="cantidad" class="form-control" onchange="getTotal()" required>
    </div>
    <div class="form-group col-6 mb-2">
      <label for="valor" class="form-label mb-0">Valor</label>
      <input type="number" name="valor" id="valor" class="form-control" onchange="getTotal()" required>
    </div>
    <div class="form-group col-6 mb-2">
      <label for="total" class="form-label mb-0">Total</label>
      <input type="number" name="total" id="total" class="form-control" readonly=readonly required>
    </div>
    <div class="d-flex align-items-end col-6 mb-2">
      <input type="submit" name="total" id="total" class="btn btn-success w-100" value="Guardar">
    </div>
  </div>
</form>

<script type="text/javascript">
  $(function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    })

    $('#form_pedido').on("submit", function(e) {
      e.preventDefault()

      let _method = $("input[name='_method']").val()
      let method = (_method) ? _method.toLowerCase() : $(this).attr("method")
      let data = Object.fromEntries(new FormData(e.target).entries())
      data.notifywebsocket = true

      $.ajax({
        url: $(this).attr("action"),
        type: method,
        data: data,
        success: function(response) {
          successNotification(response.message)
          notifyWebSocket(response.data)
          $("#form").trigger('reset')
        },
        error: function(response) {
          let {
            message,
            responseJSON
          } = response
          errorNotification(message || responseJSON?.message || 'Error guardando el pedido')
        }
      })
    })
  })

  function getTotal() {
    let cantidad = $('#cantidad').val()
    let valor = $('#valor').val()
    let total = (cantidad || 0) * (valor || 0)
    $('#total').val(total)
  }
</script>