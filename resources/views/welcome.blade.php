<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Pedidos</title>

  <!-- CDN Librerias -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
  <script src="https://cdn.socket.io/4.6.0/socket.io.min.js" integrity="sha384-c79GN5VsunZvi+Q/WObgk2in0CbZsHnjEqvFxC5DxHn9lTfNce2WW6h2pH6u/kF+" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>

<body>
  @include('_modal')
  <div class="container bg-light">
    <h1 class="fs-3 text-center">Pedidos</h1>
    <div class="row">
      <div class="col-md-6">
        <p class="fs-5">Creación de pedidos</p>
        @include('_form')
      </div>
      <div class="col-md-6"></div>
    </div>
  </div>

  <script>
    var socket = io.connect('ws://localhost:3000', {
      forceNew: true
    })
    socket.on('mensaje', (data) => console.log('Server NodeJs say: ', data))
  </script>

  <script>
    function notifyWebSocket(data) {
      console.log('message -> ', JSON.stringify(data));
      socket.emit('nuevo-pedido', JSON.stringify(data))
    }
  </script>
</body>

</html>